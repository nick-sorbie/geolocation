import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { shallow, mount, render } from 'enzyme';

function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * max) + min;
}

test('On load the average value is 0 ad the Json is an empty array', () => {
  const wrapper = mount(<App/>);

  expect(wrapper.state('averageValue')).toBe(0);

  var actual = JSON.stringify(wrapper.state('result'));
  var expected = JSON.stringify([]);
  expect(actual).toBe(expected);
});

test('The Coordinates are left unchanged, submit button is clicked, both the average and json change accordingly', () => {
  const wrapper = mount(<App/>);
  wrapper.find('.App-submit').simulate('click');

  expect(wrapper.state('averageValue')).not.toBe(0);

  var actual = JSON.stringify(wrapper.state('result'));
  var expected = JSON.stringify([]);
  expect(actual).not.toBe(expected);
});

test('The first and second and last and penultimate are selected and compared - showing decending', () => {
  const wrapper = mount(<App/>);
  wrapper.find('.App-submit').simulate('click');

  const json = wrapper.state('result');
  const lastpos = json.length - 1;
  const first = parseFloat(json[0].value);
  const second = parseFloat(json[1].value);
  const penultimate = parseFloat(json[lastpos - 1].value);
  const last = parseFloat(json[lastpos].value);

  expect(first).toBeGreaterThanOrEqual(second);
  expect(second).toBeGreaterThanOrEqual(penultimate);
  expect(penultimate).toBeGreaterThanOrEqual(last);
});

test('A random result item is selected and it has a id, value, name and email', () => {
  const wrapper = mount(<App/>);
  wrapper.find('.App-submit').simulate('click');

  const json = wrapper.state('result');
  const lastpos = json.length - 1;
  const randomPos = getRandomArbitrary(0, lastpos);
  const random = json[randomPos];

  expect(random.id).not.toBe('undefined');
  expect(random.value).not.toBe('undefined');
  expect(random.name).not.toBe('undefined');
  expect(random.email).not.toBe('undefined');
});
