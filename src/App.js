import React, { Component } from 'react';
import './App.css';
import people from './data/people';

const defaultLat = '51.450167';
const defaultLon = '-2.594678';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      lat: defaultLat,
      lon: defaultLon,
      result: [],
      averageValue: 0
    };
    this.processJson = this.processJson.bind(this);
  }

  handleTextChange = (event) => {
    this.setState({[event.target.name] : event.target.value});
  }

  processJson() {
    const values = [];
    let sum = 0;
    let counter = 0;
    const results = people.filter(item => {
      const lat = item.location.latitude, lon = item.location.longitude,
      distance = this.getHaversineDistance(lat, lon);

      if (distance <= 200) {
        counter++;
        sum += parseFloat(item.value);
      }

      return item.country === 'england' && distance <= 100;
    }).map((item) => {
      return {
        'id': item.id,
        'value': item.value,
        'name': `${item.name.first} ${item.name.last}`,
        'email': item.email
      }
    }).sort((a, b) => b.value - a.value);

    const average = sum/counter;
    return this.setState({
      'result' : results,
      'averageValue': average
    });
  }

  getHaversineDistance (lat, lon) {
    // found this Haversine fuction from a quick stackOverflow search
    Number.prototype.toRad = function() {
      return this * Math.PI / 180;
    }
    const lat1 = parseFloat(this.state.lat);
    const lon1 = parseFloat(this.state.lon);
    const lat2 = parseFloat(lat); 
    const lon2 = parseFloat(lon);
    const R = 6371; // km 
    const x1 = lat2-lat1;
    const dLat = x1.toRad();  
    const x2 = lon2-lon1;
    const dLon = x2.toRad();  
    const a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
      Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
      Math.sin(dLon/2) * Math.sin(dLon/2);  
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    const d = R * c; 

    return d;
  }
  

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">People geolocation processor</h1>
          <p>Enter coordinates below to find all the people living within 100km (England) and the average customer value of all people living within 200km</p>
        </header>
        <div className="App-main">
          <input name="lat" type="text" className="app-coordinate" value={this.state.lat} onChange={this.handleTextChange}/>
          <input name="lon" type="text" className="app-coordinate" value={this.state.lon} onChange={this.handleTextChange}/>
          <button className="App-submit" onClick={this.processJson}>Show me!</button>
        </div>
        <div className="App-results">
          <h4>{`Average customer value of all people living within 200km ${this.state.lat}, ${this.state.lon}`}</h4>
          <span className="App-average">{this.state.averageValue.toFixed(2)}</span>
        </div>
        <div className="App-results">
          <h4>{`Decending Json of people living within 100km in england of ${this.state.lat}, ${this.state.lon}`}</h4>
          <div className="App-json">
            <pre>{JSON.stringify(this.state.result, null, 2)}</pre>
          </div>    
        </div>    
      </div>
    );
  }
}

export default App;
